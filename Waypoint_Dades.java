package list1;

import java.text.Collator;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

public class Waypoint_Dades {
	private int id;                     
	private String nom;
	private int[] coordenades;
	private boolean actiu;              
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;      
	private LocalDateTime dataModificacio;
	private static int idNext=0;//esto hace que se incremente cada vez que creas uno
	
	public Waypoint_Dades(String nom, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		this.id = 0;
		this.nom = nom;
		this.coordenades = new int[] {0,0,0};
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
		//idNext++;
	}
	
	public int getId() {return id;}
	public String getNom() {return nom;}
	public int[] getCoordenades() {return coordenades;}
	public boolean isActiu() {return actiu;}
	public LocalDateTime getDataCreacio() {return dataCreacio;}
	public LocalDateTime getDataAnulacio() {return dataAnulacio;}
	public LocalDateTime getDataModificacio() {return dataModificacio;}
	
	public void setId(int id) {this.id = id;}
	public void setNom(String nom) {this.nom = nom;}
	public void setCoordenades(int[] coordenades) {this.coordenades = coordenades;}
	public void setActiu(boolean actiu) {this.actiu = actiu;}
	public void setDataCreacio(LocalDateTime dataCreacio) {this.dataCreacio = dataCreacio;}
	public void setDataAnulacio(LocalDateTime dataAnulacio) {this.dataAnulacio = dataAnulacio;}
	public void setDataModificacio(LocalDateTime dataModificacio) {this.dataModificacio = dataModificacio;}
	
	


	@Override
	public String toString() {
		return "Waypoint_Dades [id=" + id + ", nom=" + nom + ", coordenades=" + Arrays.toString(coordenades)
				+ ", actiu=" + actiu + ", dataCreacio=" + dataCreacio + ", dataAnulacio=" + dataAnulacio
				+ ", dataModificacio=" + dataModificacio + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(coordenades);
		result = prime * result + Objects.hash(nom);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Waypoint_Dades other = (Waypoint_Dades) obj;
		return Arrays.equals(coordenades, other.coordenades) && Objects.equals(nom, other.nom);
	}
    

    
    
}
