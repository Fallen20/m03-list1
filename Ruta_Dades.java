package list1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Ruta_Dades {
	private int id;                             	
	private String nom;
	private ArrayList<Integer> waypoints;       	
	private boolean actiu;                      	
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;             
	private LocalDateTime dataModificacio;
	public Ruta_Dades(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}
	public int getId() {
		return id;
	}
	public String getNom() {
		return nom;
	}
	public ArrayList<Integer> getWaypoints() {
		return waypoints;
	}
	public boolean isActiu() {
		return actiu;
	}
	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}
	public LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}
	public LocalDateTime getDataModificacio() {
		return dataModificacio;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public void setWaypoints(ArrayList<Integer> waypoints) {
		this.waypoints = waypoints;
	}
	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}
	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}
	public void setDataAnulacio(LocalDateTime dataAnulacio) {
		this.dataAnulacio = dataAnulacio;
	}
	public void setDataModificacio(LocalDateTime dataModificacio) {
		this.dataModificacio = dataModificacio;
	}
	@Override
	public String toString() {
		return "Ruta_Dades [id=" + id + ", nom=" + nom + ", waypoints=" + waypoints + ", actiu=" + actiu
				+ ", dataCreacio=" + dataCreacio + ", dataAnulacio=" + dataAnulacio + ", dataModificacio="
				+ dataModificacio + "]";
	}
    
    
    
}
