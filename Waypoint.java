package list1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Waypoint {
	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		ComprovacioRendiment comprovacioRendimentTmp=new ComprovacioRendiment();
		System.out.println("Metodo hecho con exito");
		return comprovacioRendimentTmp;
	}
	
	public static ComprovacioRendiment comprovarRendimentInicialitzacio
					(int numObjACrear,ComprovacioRendiment comprovacioRendimentTmp) {
		List<Waypoint_Dades> listaArray=new ArrayList<Waypoint_Dades>();
		List<Waypoint_Dades> listaLinked=new LinkedList<Waypoint_Dades>();
		DateTimeFormatter fecha = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		
		
		long empezar=System.nanoTime();
		for(int contador=0;contador<numObjACrear;contador++) {
			Waypoint_Dades aDades= new Waypoint_Dades("Orbita de la terra", true, LocalDateTime.parse("15-08-1954 00:01", fecha), null, null);
			listaArray.add(aDades);
		}
		long finale=System.nanoTime();
		long duracion=finale-empezar;
		//Collections.copy(comprovacioRendimentTmp.llistaArrayList,listaArray);//MIRAR EN CLASE, EN CASA NO FUNCIONA
		comprovacioRendimentTmp.setLlistaArrayList(listaArray);//le asignas la arraycreada
		System.out.println("Tiempo en crear "+numObjACrear+" objetos con arrayList: "+TimeUnit.NANOSECONDS.toMillis(duracion));
		
		empezar=System.nanoTime();
		for(int contador=0;contador<numObjACrear;contador++) {
			Waypoint_Dades aDades= new Waypoint_Dades("Orbita de la terra", true, LocalDateTime.parse("15-08-1954 00:01", fecha), null, null);
			listaLinked.add(aDades);
		}
		finale=System.nanoTime();
		duracion=finale-empezar;
		//System.out.println(listaLinked.toString());
		
		comprovacioRendimentTmp.setLlistaLinkedList(listaLinked);
		System.out.println("Tiempo en crear "+numObjACrear+" objetos con LinkedList: "+TimeUnit.NANOSECONDS.toMillis(duracion));
		
		return comprovacioRendimentTmp;
	}
	
	public static  ComprovacioRendiment comprovarRendimentInsercio(
							ComprovacioRendiment comprovacioRendimentTmp) {
		DateTimeFormatter fecha = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		Waypoint_Dades aDades= new Waypoint_Dades("Orbita de la terra", true, LocalDateTime.parse("15-08-1954 00:01", fecha), null, null);
		
		//tiempo
		long principio=System.nanoTime();
		long finale=System.nanoTime();
		long duracion=0;
		
		int posicion=comprovacioRendimentTmp.getLlistaArrayList().size();
		
		System.out.println("size: "+comprovacioRendimentTmp.getLlistaArrayList().size()+ " --- posicion del medio: "+comprovacioRendimentTmp.getLlistaLinkedList().size()/2);
		
		principio=System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.set(0, aDades);//primera
		finale=System.nanoTime();
		duracion=finale-principio;
		System.out.println("Tiempo en insertar 1 waypoint en la posicion 0 de la arrayList: "+TimeUnit.NANOSECONDS.toMillis(duracion));

		principio=System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.set(0, aDades);
		finale=System.nanoTime();
		duracion=finale-principio;
		System.out.println("Tiempo en insertar 1 waypoint en la posicion 0 de la LinkedList: "+TimeUnit.NANOSECONDS.toMillis(duracion));
		System.out.println("------------------------------");
		
		
		
		principio=System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.set(posicion/2, aDades);//primera
		finale=System.nanoTime();
		duracion=finale-principio;
		System.out.println("Tiempo en insertar 1 waypoint en la posicion "+posicion/2+" de la arrayList: "+TimeUnit.NANOSECONDS.toMillis(duracion));

		principio=System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.set(posicion/2, aDades);//primera
		finale=System.nanoTime();
		duracion=finale-principio;
		System.out.println("Tiempo en insertar 1 waypoint en la posicion "+posicion/2+" de la LinkedList: "+TimeUnit.NANOSECONDS.toMillis(duracion));
		System.out.println("------------------------------");
		
		
		
		
		principio=System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.set(posicion-1, aDades);//primera
		finale=System.nanoTime();
		duracion=finale-principio;
		System.out.println("Tiempo en insertar 1 waypoint en la posicion "+posicion+" de la arrayList: "+TimeUnit.NANOSECONDS.toMillis(duracion));

		principio=System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.set(posicion-1, aDades);//primera
		finale=System.nanoTime();
		duracion=finale-principio;
		System.out.println("Tiempo en insertar 1 waypoint en la posicion "+posicion+" de la LinkedList: "+TimeUnit.NANOSECONDS.toMillis(duracion));
		
		return comprovacioRendimentTmp;
	}
	
	public static  ComprovacioRendiment modificarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		List<Integer> idsPerArrayList=new ArrayList<Integer>();
		for(int contador=0;contador<comprovacioRendimentTmp.getLlistaArrayList().size();contador++) {idsPerArrayList.add(contador);}
		
		System.out.println("---------Apartado 1--------");
		System.out.println("Se ha iniciado la lista 'idsPerArrayList' con "+idsPerArrayList.size()+" elementos");
		System.out.println("El 1er elemento tiene el valor de "+idsPerArrayList.get(0));
		System.out.println("El ultimo elemento tiene el valor de "+idsPerArrayList.get(idsPerArrayList.size()-1));
		
		
		System.out.println("\n---------Apartado 2--------");
		
		for(Integer a:idsPerArrayList) {//a ya es un contador normal
			int idNuevo=idsPerArrayList.get(a);
			int id= comprovacioRendimentTmp.getLlistaArrayList().get(a).getId();
			System.out.println("ANTES DEL CAMBIO: comprovacioRendimentTmp.getLlistaArrayList().get("+a+").getId() ="+
					id);
			
			comprovacioRendimentTmp.getLlistaArrayList().get(a).setId(idNuevo);
			
			id= comprovacioRendimentTmp.getLlistaArrayList().get(a).getId();
			System.out.println("DESPUES DEL CAMBIO: comprovacioRendimentTmp.getLlistaArrayList().get(\"+contador+\").getId() = "
								+id);
			System.out.println();

		}
		System.out.println("\n---------Apartado 3.1 (bucle for)--------");
		for(Integer a: idsPerArrayList) {
			System.out.println("ID= "+comprovacioRendimentTmp.getLlistaArrayList().get(a).getId()
								+", nom= "+comprovacioRendimentTmp.getLlistaArrayList().get(a).getNom());
		}
		
		Iterator <Waypoint_Dades> iter = comprovacioRendimentTmp.getLlistaArrayList().iterator();//esto hace el iterator de la arraylist
		//Iterator iter = idsPerArrayList.iterator(); // esto hace un iterator de la lista general y no puedes ir dentro
		System.out.println("\n---------Apartado 3.2 (Iterator)--------");
		Waypoint_Dades iteradorElemento;
		while (iter.hasNext()) {
			iteradorElemento=iter.next();//por alguna razon si no lo guardas en una var te salta una posicion
			
			System.out.println("ID= "+iteradorElemento.getId()+", nom= "+iteradorElemento.getNom());
		}
		
		System.out.println("\n---------Apartado 4--------");
		System.out.println("Preparado para borrar el contenido de la lista listaLinkedList que tiene "+comprovacioRendimentTmp.getLlistaLinkedList().size()+" elementos");
		comprovacioRendimentTmp.getLlistaLinkedList().clear();//lo borras
		System.out.println("Borrada! Ahora la lista tiene "+comprovacioRendimentTmp.getLlistaLinkedList().size()+" elementos");
		comprovacioRendimentTmp.getLlistaLinkedList().addAll(comprovacioRendimentTmp.getLlistaArrayList());//copia
		System.out.println("Copiados los elementos de llistaArrayList a llistaLinked list y ahora tiene "+comprovacioRendimentTmp.getLlistaLinkedList().size()+" elementos");
		
		
		System.out.println("\n---------Apartado 5.1 (bucle for)--------");
		for(Waypoint_Dades a: comprovacioRendimentTmp.getLlistaArrayList()) {
			
			if(a.getId()>5) {
				a.setNom("Orbita de Mart");
				System.out.println("Se ha modificado el waypoint con id "+a.getId());
			}
		}
		
		System.out.println("\n---------Apartado 5.1 (comprovacion)--------");
		for(int contador=0;contador<comprovacioRendimentTmp.getLlistaArrayList().size();contador++) {
			System.out.println("ID= "+comprovacioRendimentTmp.getLlistaArrayList().get(contador).getId()
					+", nom= "+comprovacioRendimentTmp.getLlistaArrayList().get(contador).getNom());
		}
		
		System.out.println("\n---------Apartado 5.2 (Iterator)--------");
		iter = comprovacioRendimentTmp.getLlistaArrayList().iterator();
		while (iter.hasNext()) {
			iteradorElemento=iter.next();//por alguna razon si no lo guardas en una var te salta una posicion
			
			if(iteradorElemento.getId()<5) {
				iteradorElemento.setNom("Punt Lagrange entre la Terra i la LLuna");
				System.out.println("Se ha modificado el waypoint con id "+iteradorElemento.getId());
			}
		}
		
		System.out.println("\n---------Apartado 5.2 (comprovacion)--------");
		for(int contador=0;contador<comprovacioRendimentTmp.getLlistaArrayList().size();contador++) {
			System.out.println("ID= "+comprovacioRendimentTmp.getLlistaArrayList().get(contador).getId()
					+", nom= "+comprovacioRendimentTmp.getLlistaArrayList().get(contador).getNom());
		}
		return comprovacioRendimentTmp;
	}
	
	public static  ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		Waypoint_Dades iteradorElemento;
		int contador=0;
		/*
		for(Waypoint_Dades a: comprovacioRendimentTmp.getLlistaArrayList()) {
			if(a.getId()<6) {
				System.out.println("Se ha eliminado el waypoint numero "+a.getId());
				contador++;
				comprovacioRendimentTmp.getLlistaArrayList().remove(contador);	
			}
			
		}*/
		System.out.println("\n---------Apartado 1--------");
		System.out.println("No se ha podido eliminar porque has de empezar borrando desde el elemento final, no desde el primero");
		
		System.out.println("\n---------Apartado 2 (Iterator)--------");
		Iterator <Waypoint_Dades> iter = comprovacioRendimentTmp.getLlistaLinkedList().iterator();//esto hace el iterator de la arraylist
		while(iter.hasNext()) {
			iteradorElemento=iter.next();
			if(iteradorElemento.getId()>4) {
				System.out.println("Se ha eliminado el waypoint con id ="+ iteradorElemento.getId());
				iter.remove();
				
			}
		}
		
		
		System.out.println("\n---------Apartado 2 (comprovacion)--------");
		for(int contador2=0;contador2<comprovacioRendimentTmp.getLlistaLinkedList().size();contador2++) {
			System.out.println("El waypoint con id = "+comprovacioRendimentTmp.getLlistaArrayList().get(contador2).getId()
					+" tiene el nombre = "+comprovacioRendimentTmp.getLlistaArrayList().get(contador2).getNom());
		}
		
		
		System.out.println("\n---------Apartado 3 (ListIterator)--------");
		ListIterator<Waypoint_Dades> iterList = comprovacioRendimentTmp.getLlistaLinkedList().listIterator();//esto hace el iterator de la arraylist
			while(iterList.hasNext()) {
				iteradorElemento=iterList.next();
				
				if(iteradorElemento.getId()==2) {
					System.out.println("Se ha eliminado el waypoint con id ="+ iteradorElemento.getId());
					iterList.remove();
					
				}
			}
			
			
			System.out.println("\n---------Apartado 3 (comprovacion)--------");
			iterList = comprovacioRendimentTmp.getLlistaLinkedList().listIterator(comprovacioRendimentTmp.getLlistaLinkedList().size()-1);
			//el ultimo() dice el tamano. Si no le pones nada, empieza desde el 0. POnerle [..].size()-1 es el final
			while(iterList.hasPrevious()) {
				iteradorElemento=iterList.previous();
				System.out.println("El waypoint con id = "+iteradorElemento.getId()
						+" tiene el nombre = "+iteradorElemento.getNom());
				 
			
	}
		return comprovacioRendimentTmp;
		
	}
	
}
