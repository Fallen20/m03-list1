package list1;

import java.util.*;

public class ComprovacioRendiment {
	int[] coordenadesTmp;
	List<Waypoint_Dades> llistaArrayList;
	List<Waypoint_Dades> llistaLinkedList;
	
	public ComprovacioRendiment() {
		this.coordenadesTmp = new int[] {0,0,0};
		this.llistaArrayList = new ArrayList <Waypoint_Dades>();
		this.llistaLinkedList = new ArrayList <Waypoint_Dades>();
	}

	public int[] getCoordenadesTmp() {return coordenadesTmp;}
	public List<Waypoint_Dades> getLlistaArrayList() {return llistaArrayList;}
	public List<Waypoint_Dades> getLlistaLinkedList() {return llistaLinkedList;}

	public void setCoordenadesTmp(int[] coordenadesTmp) {this.coordenadesTmp = coordenadesTmp;}
	public void setLlistaArrayList(List<Waypoint_Dades> llistaArrayList) {this.llistaArrayList = llistaArrayList;}
	public void setLlistaLinkedList(List<Waypoint_Dades> llistaLinkedList) {this.llistaLinkedList = llistaLinkedList;}

	@Override
	public String toString() {
		return "ComprovacioRendiment [coordenadesTmp=" + Arrays.toString(coordenadesTmp) + ", llistaArrayList="
				+ llistaArrayList + ", llistaLinkedList=" + llistaLinkedList + "]";
	}
	
	
}
